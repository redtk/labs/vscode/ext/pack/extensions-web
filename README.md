# redtk.redtk-extensions-web

## Installation

Launch VS Code Quick Open (Ctrl+P), paste the following command, and press enter.

```text
ext install redtk.redtk-extensions-web
```

## Prepared for
Web projects.
